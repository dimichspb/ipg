<?php

use yii\db\Migration;

/**
 * Handles adding deleted to table `{{%user}}`.
 */
class m201126_043822_add_deleted_columns_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%user}}', 'created_at', $this->integer()->after('{{%id}}'));
        $this->alterColumn('{{%user}}', 'updated_at', $this->integer()->after('{{%id}}'));
        $this->addColumn('{{%user}}', 'deleted_at', $this->integer()->after('updated_by'));
        $this->addColumn('{{%user}}', 'deleted_by', $this->integer()->after('deleted_at'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'deleted_by');
        $this->dropColumn('{{%user}}', 'deleted_at');
    }
}
