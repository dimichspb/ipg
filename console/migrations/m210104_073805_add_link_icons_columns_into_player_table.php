<?php

use yii\db\Migration;

/**
 * Class m210104_073805_add_link_icons_columns_into_player_table
 */
class m210104_073805_add_link_icons_columns_into_player_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%player}}', 'link_a_icon', $this->string()->after('link_a'));
        $this->addColumn('{{%player}}', 'link_b_icon', $this->string()->after('link_b'));
        $this->addColumn('{{%player}}', 'link_c_icon', $this->string()->after('link_c'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%player}}', 'link_a_icon');
        $this->dropColumn('{{%player}}', 'link_b_icon');
        $this->dropColumn('{{%player}}', 'link_c_icon');
    }
}
