<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%match}}`.
 */
class m201124_043753_create_match_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%match}}', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_at' => $this->integer(),
            'updated_by' => $this->integer(),
            'deleted_at' => $this->integer(),
            'deleted_by' => $this->integer(),
            'status' => $this->integer(),
            'datetime' => $this->integer(),
            'tournament_id' => $this->integer(),
            'team_a_id' => $this->integer(),
            'team_b_id' => $this->integer(),
            'score_a' => $this->integer(),
            'score_b' => $this->integer(),
        ]);
        $this->addForeignKey('fk_match_user_created_by', '{{%match}}', 'created_by', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_match_user_updated_by', '{{%match}}', 'updated_by', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_match_user_deleted_by', '{{%match}}', 'deleted_by', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
        
        $this->addForeignKey('fk_match_tournament_tournament_id', '{{%match}}', 'tournament_id', '{{%tournament}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_match_teams_team_a_id', '{{%match}}', 'team_a_id', '{{%team}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_match_teams_team_b_id', '{{%match}}', 'team_b_id', '{{%team}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%match}}');
    }
}
