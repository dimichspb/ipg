<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%tournament}}`.
 */
class m201124_043653_create_tournament_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%tournament}}', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_at' => $this->integer(),
            'updated_by' => $this->integer(),
            'deleted_at' => $this->integer(),
            'deleted_by' => $this->integer(),
            'status' => $this->integer(),
            'name' => $this->string(),
            'image' => $this->string(),
            'game_id' => $this->integer(),
        ]);
        $this->addForeignKey('fk_tournament_user_created_by', '{{%tournament}}', 'created_by', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_tournament_user_updated_by', '{{%tournament}}', 'updated_by', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_tournament_user_deleted_by', '{{%tournament}}', 'deleted_by', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
        
        $this->addForeignKey('fk_tournament_games_game_id', '{{%tournament}}', 'game_id', '{{%game}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%tournament}}');
    }
}
