<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%user}}`.
 */
class m201126_043720_add_updated_by_created_by_columns_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'created_by', $this->integer()->after('created_at'));
        $this->addColumn('{{%user}}', 'updated_by', $this->integer()->after('updated_at'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'created_by');
        $this->dropColumn('{{%user}}', 'updated_by');
    }
}
