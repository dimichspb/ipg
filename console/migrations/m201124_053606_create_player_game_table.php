<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%player_game}}`.
 */
class m201124_053606_create_player_game_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%player_game}}', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_at' => $this->integer(),
            'updated_by' => $this->integer(),
            'deleted_at' => $this->integer(),
            'deleted_by' => $this->integer(),
            'status' => $this->integer(),
            'player_id' => $this->integer(),
            'game_id' => $this->integer(),
        ]);
        $this->addForeignKey('fk_player_game_user_created_by', '{{%player_game}}', 'created_by', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_player_game_user_updated_by', '{{%player_game}}', 'updated_by', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_player_game_user_deleted_by', '{{%player_game}}', 'deleted_by', '{{%user}}', 'id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('fk_player_game_player_player_id', '{{%player_game}}', 'player_id', '{{%player}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_player_game_game_game_id', '{{%player_game}}', 'game_id', '{{%game}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%player_game}}');
    }
}
