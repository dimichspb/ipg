<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%player}}`.
 */
class m201124_053448_create_player_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%player}}', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_at' => $this->integer(),
            'updated_by' => $this->integer(),
            'deleted_at' => $this->integer(),
            'deleted_by' => $this->integer(),
            'status' => $this->integer(),
            'name' => $this->string(),
            'description' => $this->text(),
            'image' => $this->string(),
            'link_a' => $this->string(),
            'link_b' => $this->string(),
            'link_c' => $this->string(),
        ]);
        $this->addForeignKey('fk_player_user_created_by', '{{%player}}', 'created_by', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_player_user_updated_by', '{{%player}}', 'updated_by', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_player_user_deleted_by', '{{%player}}', 'deleted_by', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%player}}');
    }
}
