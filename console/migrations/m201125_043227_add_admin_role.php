<?php

use yii\db\Migration;
use yii\rbac\ManagerInterface;

/**
 * Class m201125_043227_add_admin_role
 */
class m201125_043227_add_admin_role extends Migration
{
    /**
     * @var ManagerInterface
     */
    protected $authManager;

    public function __construct(ManagerInterface $authManager, $config = [])
    {
        $this->authManager = $authManager;

        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     * @throws Exception
     */
    public function safeUp()
    {
        $role = $this->authManager->createRole('admin');
        $role->description = 'Application Administrator';
        $this->authManager->add($role);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $role = $this->authManager->getRole('admin');
        $this->authManager->remove($role);
    }
}
