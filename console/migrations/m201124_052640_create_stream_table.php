<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%stream}}`.
 */
class m201124_052640_create_stream_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%stream}}', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_at' => $this->integer(),
            'updated_by' => $this->integer(),
            'deleted_at' => $this->integer(),
            'deleted_by' => $this->integer(),
            'status' => $this->integer(),
            'name' => $this->string(),
            'description' => $this->text(),
            'url' => $this->string(),
        ]);
        $this->addForeignKey('fk_stream_user_created_by', '{{%stream}}', 'created_by', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_stream_user_updated_by', '{{%stream}}', 'updated_by', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_stream_user_deleted_by', '{{%stream}}', 'deleted_by', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%stream}}');
    }
}
