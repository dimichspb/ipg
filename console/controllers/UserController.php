<?php
namespace console\controllers;

use common\models\Status;
use common\models\User;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\Json;
use yii\rbac\ManagerInterface;

class UserController extends Controller
{
    /**
     * @var ManagerInterface
     */
    protected $authManager;

    /**
     * UserController constructor.
     * @param $id
     * @param $module
     * @param ManagerInterface $authManager
     * @param array $config
     */
    public function __construct($id, $module, ManagerInterface $authManager, $config = [])
    {
        $this->authManager = $authManager;

        parent::__construct($id, $module, $config);
    }

    /**
     * @param string $username
     * @return int
     * @throws \Exception
     */
    public function actionAdmin(string $username)
    {
        $role = $this->authManager->getRole('admin');
        $user = User::findByUsername($username);

        if (!$user) {
            $this->stderr(\Yii::t('app', 'User "{username}" not found', [
                'username' => $username,
            ]));
            return ExitCode::NOUSER;
        }

        if (!$this->authManager->assign($role, $user->id)) {
            $this->stderr(\Yii::t('app', 'Cannot assign Role "{role}" to the User "{username}"', [
                'role' => $role->name,
                'username' => $user->username,
            ]));
            return ExitCode::CONFIG;
        }

        $this->stdout(\Yii::t('app', 'Role "{role}" has been assigned to the User "{username}" successfully', [
            'role' => $role->name,
            'username' => $user->username,
        ]));

        return ExitCode::OK;
    }

    /**
     * @param string $username
     * @param string $email
     * @param string $password
     * @return int
     */
    public function actionCreate(string $username, string $email, string $password)
    {
        $user = new User();
        $user->username = $username;
        $user->email = $email;
        $user->status = Status::STATUS_ACTIVE;
        $user->setPassword($password);
        $user->generateAuthKey();

        if (!$user->save()) {
            $this->stderr(\Yii::t('app', 'Error creating User: {errors}', [
                'errors' => Json::encode($user->getErrorSummary(true)),
            ]));
            return ExitCode::CONFIG;
        }

        $this->stdout(\Yii::t('app', 'User has been created successfully'));

        return ExitCode::OK;
    }
}