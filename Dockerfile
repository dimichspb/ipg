FROM php:7.4-fpm

RUN apt-get update && apt-get install -y

RUN apt-get update && apt-get install -y --no-install-recommends \
        git \
        zlib1g-dev \
        libxml2-dev \
        libzip-dev \
        libjpeg-dev \
        libpng-dev

RUN docker-php-ext-configure gd --with-jpeg=/usr/include/

RUN docker-php-ext-install \
        zip \
        gd \
        intl \
        mysqli \
        pdo pdo_mysql

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

CMD composer install ; php init --env=Production --overwrite=All; php yii migrate --interactive=0; php-fpm;

WORKDIR /var/www/html/
