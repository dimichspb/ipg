<?php

namespace frontend\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\News;

/**
 * NewsSearch represents the model behind the search form of `common\models\News`.
 */
class NewsSearch extends Model
{
    public $created_at_start;
    public $created_at_end;
    public $created_by;
    public $updated_at_start;
    public $updated_at_end;
    public $updated_by;
    public $deleted_at_start;
    public $deleted_at_end;
    public $deleted_by;
    public $status;
    public $title;
    public $body;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at_start', 'created_at_end', 'updated_at_start', 'updated_at_end', 'deleted_at_start', 'deleted_at_end', 'status'], 'integer'],
            [['title', 'body', 'created_by', 'updated_by', 'deleted_by',], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = News::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $like = News::getDb()->driverName === 'pgsql'? 'ILIKE': 'LIKE';

        // grid filtering conditions
        $query->andFilterWhere(['>=', 'created_at', $this->created_at_start,]);
        $query->andFilterWhere(['<=', 'created_at', $this->created_at_end,]);
        $query->andFilterWhere(['>=', 'updated_at', $this->updated_at_start,]);
        $query->andFilterWhere(['<=', 'updated_at', $this->updated_at_end,]);
        $query->andFilterWhere(['>=', 'deleted_at', $this->deleted_at_start,]);
        $query->andFilterWhere(['<=', 'deleted_at', $this->deleted_at_end,]);
        $query->andFilterWhere([$like, 'created_by', $this->created_by]);
        $query->andFilterWhere([$like, 'updated_by', $this->updated_by]);
        $query->andFilterWhere([$like, 'deleted_by', $this->deleted_by]);
        $query->andFilterWhere([$like, 'title', $this->title]);
        $query->andFilterWhere([$like, 'body', $this->body]);

        $query->andFilterWhere(['status' => $this->status,]);

        $dataProvider->sort->defaultOrder = ['created_at' => SORT_ASC];

        return $dataProvider;
    }
}
