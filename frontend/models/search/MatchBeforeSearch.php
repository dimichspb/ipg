<?php

namespace frontend\models\search;

use common\models\Game;
use common\models\Tournament;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Match;

/**
 * MatchBeforeSearch represents the model behind the search form of `common\models\Match`.
 */
class MatchBeforeSearch extends Model
{
    public $created_at_start;
    public $created_at_end;
    public $created_by;
    public $updated_at_start;
    public $updated_at_end;
    public $updated_by;
    public $deleted_at_start;
    public $deleted_at_end;
    public $deleted_by;
    public $datetime_start;
    public $datetime_end;
    public $status;
    public $tournament_id;
    public $game_id;
    public $team_a_id;
    public $team_b_id;
    public $score_a;
    public $score_b;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'created_at_start', 'created_at_end', 'updated_at_start', 'updated_at_end', 'deleted_at_start', 'deleted_at_end',
                    'datetime_start', 'datetime_end', 'tournament_id', 'game_id', 'team_a_id', 'team_b_id', 'score_a', 'score_b', 'status',
                    'created_by', 'updated_by', 'deleted_by',
                ], 'integer'
            ],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Match::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $like = Match::getDb()->driverName === 'pgsql'? 'ILIKE': 'LIKE';

        // grid filtering conditions
        $query->andFilterWhere(['>=', 'created_at', $this->created_at_start,]);
        $query->andFilterWhere(['<=', 'created_at', $this->created_at_end,]);
        $query->andFilterWhere(['>=', 'updated_at', $this->updated_at_start,]);
        $query->andFilterWhere(['<=', 'updated_at', $this->updated_at_end,]);
        $query->andFilterWhere(['>=', 'deleted_at', $this->deleted_at_start,]);
        $query->andFilterWhere(['<=', 'deleted_at', $this->deleted_at_end,]);
        $query->andFilterWhere(['>=', 'datetime', $this->datetime_start]);
        $query->andFilterWhere(['<=', 'datetime', $this->datetime_end]);
        $query->andFilterWhere(['created_by' => $this->created_by]);
        $query->andFilterWhere(['updated_by' => $this->updated_by]);
        $query->andFilterWhere(['deleted_by' => $this->deleted_by]);
        $query->andFilterWhere([Tournament::tableName() . '.id' => $this->tournament_id]);
        $query->andFilterWhere([Game::tableName() . '.id' => $this->game_id]);
        $query->andFilterWhere(['team_a_id' => $this->team_a_id]);
        $query->andFilterWhere(['team_b_id' => $this->team_b_id]);
        $query->andFilterWhere(['score_a' => $this->score_a]);
        $query->andFilterWhere(['score_b' => $this->score_b]);
        $query->andFilterWhere(['status' => $this->status,]);

        $query->andWhere(['<=', 'datetime', (new \DateTime())->getTimestamp()]);

        $dataProvider->sort->defaultOrder = ['datetime' => SORT_ASC];

        return $dataProvider;
    }
}
