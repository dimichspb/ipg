<?php

namespace frontend\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Partner;

/**
 * PartnerSearch represents the model behind the search form of `common\models\Partner`.
 */
class PartnerSearch extends Model
{
    public $created_at_start;
    public $created_at_end;
    public $created_by;
    public $updated_at_start;
    public $updated_at_end;
    public $updated_by;
    public $deleted_at_start;
    public $deleted_at_end;
    public $deleted_by;
    public $status;
    public $name;
    public $description;
    public $url;
    public $link_a;
    public $link_b;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'created_at_start', 'created_at_end', 'updated_at_start', 'updated_at_end', 'deleted_at_start', 'deleted_at_end',
                    'created_by', 'updated_by', 'deleted_by', 'status',
                ], 'integer'
            ],
            [['name', 'description', 'url', 'link_a', 'link_b'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Partner::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $like = Partner::getDb()->driverName === 'pgsql'? 'ILIKE': 'LIKE';

        // grid filtering conditions
        $query->andFilterWhere(['>=', 'created_at', $this->created_at_start,]);
        $query->andFilterWhere(['<=', 'created_at', $this->created_at_end,]);
        $query->andFilterWhere(['>=', 'updated_at', $this->updated_at_start,]);
        $query->andFilterWhere(['<=', 'updated_at', $this->updated_at_end,]);
        $query->andFilterWhere(['>=', 'deleted_at', $this->deleted_at_start,]);
        $query->andFilterWhere(['<=', 'deleted_at', $this->deleted_at_end,]);
        $query->andFilterWhere(['created_by' => $this->created_by]);
        $query->andFilterWhere(['updated_by' => $this->updated_by]);
        $query->andFilterWhere(['deleted_by' => $this->deleted_by]);
        $query->andFilterWhere([$like, 'name', $this->name]);
        $query->andFilterWhere([$like, 'description', $this->description]);
        $query->andFilterWhere([$like, 'url', $this->url]);
        $query->andFilterWhere([$like, 'link_a', $this->link_a]);
        $query->andFilterWhere([$like, 'link_b', $this->link_b]);

        $query->andFilterWhere(['status' => $this->status,]);

        $dataProvider->sort->defaultOrder = ['created_at' => SORT_ASC];

        return $dataProvider;
    }
}
