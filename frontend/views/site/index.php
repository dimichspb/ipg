<?php

/** @var $this View */
/** @var $newsJumbotronDataProvider DataProviderInterface */
/** @var $matchBeforeDataProvider DataProviderInterface */
/** @var $matchAfterDataProvider DataProviderInterface */
/** @var $partnerDataProvider DataProviderInterface */
/** @var $streamDataProvider DataProviderInterface */
/** @var $newsDataProvider DataProviderInterface */
/** @var $gameDataProvider DataProviderInterface */
/** @var $playerDataProvider DataProviderInterface */

use rmrevin\yii\fontawesome\AssetBundle;
use yii\data\DataProviderInterface;
use yii\web\View;

$this->title = Yii::$app->name;

AssetBundle::register($this);
?>
<div class="site-index">
    <?= $this->render('index/_jumbotron', [
        'dataProvider' => $newsJumbotronDataProvider,
    ]); ?>
    <div class="body-content">
        <?= $this->render('index/_partners', [
            'dataProvider' => $partnerDataProvider,
        ]); ?>
        <?= $this->render('index/_matches', [
            'beforeDataProvider' => $matchBeforeDataProvider,
            'afterDataProvider' => $matchAfterDataProvider,
        ]); ?>
        <?= $this->render('index/_streams', [
            'dataProvider' => $streamDataProvider,
        ]); ?>
        <?= $this->render('index/_news', [
            'dataProvider' => $newsDataProvider,
        ]); ?>
        <?= $this->render('index/_players', [
            'gameDataProvider' => $gameDataProvider,
            'playerDataProvider' => $playerDataProvider,
        ]); ?>
        <?= $this->render('index/_support', [
            'dataProvider' => $partnerDataProvider,
        ]); ?>
    </div>
</div>
