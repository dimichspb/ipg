<?php

/** @var $this View */
/** @var $beforeDataProvider DataProviderInterface */
/** @var $afterDataProvider DataProviderInterface */

use common\models\Match;
use yii\bootstrap\Html;
use yii\data\DataProviderInterface;
use yii\web\View;
use yii\widgets\ListView;

$this->registerJs(
<<<JS
$(document).ready(function() {
    let buttons = $(".matches-choice-btn");
    
    buttons.each(function() {
        let button = $(this),
            active = button.hasClass('active'),
            containerId = button.data('container'),
            container = $("#" + containerId);
        
        container.toggle(active);
    })
})
$(document).on('click', '.matches-choice-btn', function() {
    let button = $(this),
        buttons = $(".matches-choice-btn"),
        containers = $(".matches-container"),
        containerId = button.data('container');
    
    buttons.each(function() {
        $(this).removeAttr('active');
    })
    
    button.addClass('active');
    
    containers.each(function() {
        let container = $(this);
        
        container.toggle(container.attr('id') === containerId);
    });
    
    return false;
})
JS
);

?>
<div class="site-index-matches">
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 text-center">
            <h3>Матчи</h3>
        </div>
    </div>
    <div class="matches-choice-btn-container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                <div class="btn-group">
                    <?= Html::a(
                        'Прошедшие',
                        '#',
                        [
                            'class' => 'btn btn-default matches-choice-btn active',
                            'data' => [
                                'container' => 'before-matches-container',
                            ]
                        ]
                    ); ?>
                    <?= Html::a(
                        'Предстоящие',
                        '#',
                        [
                            'class' => 'btn btn-default matches-choice-btn',
                            'data' => [
                                'container' => 'after-matches-container',
                            ]
                        ]
                    ); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="matches-list-container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 text-left matches-container" id="before-matches-container">
                <?= ListView::widget([
                    'dataProvider' => $beforeDataProvider,
                    'itemView' => function(Match $model) {
                        return $this->render('matches/_before_item', [
                            'model' => $model,
                        ]);
                    },
                    'layout' => "{items}",
                ]); ?>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-12 text-left matches-container" id="after-matches-container">
                <?= ListView::widget([
                    'dataProvider' => $afterDataProvider,
                    'itemView' => function(Match $model) {
                        return $this->render('matches/_after_item', [
                            'model' => $model,
                        ]);
                    },
                    'layout' => "{items}",
                ]); ?>
            </div>
        </div>
    </div>
</div>
