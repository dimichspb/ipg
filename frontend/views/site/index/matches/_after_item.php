<?php

/** @var $this View */
/** @var $model Match */

use common\models\Match;
use yii\bootstrap\Html;
use yii\web\View;

?>
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="matches-item">
            <div class="row vertical-align">
                <div class="col-sm-12 col-md-2 col-lg-2"></div>
                <div class="col-sm-12 col-md-2 col-lg-2">
                    <div class="row vertical-align">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?= \Yii::$app->formatter->asDate($model->datetime); ?>&nbsp;<span class="label label-default"><?= Html::encode($model->tournament->game->name); ?></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <h3><?= Html::encode($model->tournament->name); ?></h3>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-2 col-lg-2">
                    <div class="row vertical-align">
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <?= Html::img($model->teamA->getImage()->getUrl('100px'), ['alt' => $model->teamA->name, 'class' => 'img-thumbnail']); ?>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <h4><?= Html::encode($model->teamA->name); ?></h4>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-2 col-lg-2">
                    <div class="row">
                        <div class="col-sm-4 col-md-4 col-lg-4">
                            <span class="score"></span>
                        </div>
                        <div class="col-sm-4 col-md-4 col-lg-4">
                            <span class="score"></span>
                        </div>
                        <div class="col-sm-4 col-md-4 col-lg-4">
                            <span class="score"></span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-2 col-lg-2">
                    <div class="row vertical-align">
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <h4><?= Html::encode($model->teamB->name); ?></h4>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <?= Html::img($model->teamB->getImage()->getUrl('100px'), ['alt' => $model->teamB->name, 'class' => 'img-thumbnail']); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-2 col-lg-2"></div>
            </div>
        </div>
    </div>
</div>
