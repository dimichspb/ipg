<?php

/** @var $this View */
/** @var $dataProvider DataProviderInterface */

use common\models\News;
use yii\data\DataProviderInterface;
use yii\web\View;
use yii\widgets\ListView;

?>
<div class="site-index-news">
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 text-center text-sm-center">
            <h3>Свежие новости</h3>
        </div>
        <div class="col-sm-12 col-md-1 col-lg-1"></div>
        <div class="col-sm-12 col-md-10 col-lg-10 text-center text-sm-center">
            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => function(News $model) {
                    return $this->render('news/_item', [
                        'model' => $model,
                    ]);
                },
                'layout' => "{items}",
            ]); ?>
        </div>
        <div class="col-sm-12 col-md-1 col-lg-1"></div>
    </div>
</div>
