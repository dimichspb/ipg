<?php

/** @var $this View */
/** @var $dataProvider DataProviderInterface */

use common\models\News;
use yii\data\DataProviderInterface;
use yii\web\View;
use yii\widgets\ListView;

?>
<div class="site-index-jumbotron">
    <div class="row">
        <div class="col-sm-12 col-md-6 col-lg-6 text-left text-sm-center">
            <h2>МЫ - РОССИЙСКАЯ КИБЕРСПОРТИВНАЯ ОРГАНИЗАЦИЯ</h2>
            <h1>IMPERIAL PRO GAMING</h1>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 text-left text-sm-center">
                    <h3>Свежие новости</h3>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-12 text-left text-sm-center">
                    <?= ListView::widget([
                        'dataProvider' => $dataProvider,
                        'itemView' => function(News $model) {
                            return $this->render('jumbotron/_item', [
                                'model' => $model,
                            ]);
                        },
                        'layout' => "{items}",
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
