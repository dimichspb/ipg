<?php

/** @var $this View */
/** @var $model Player */

use common\models\Player;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\web\View;
use yii\helpers\Json;

?>
<div class="col-sm-12 col-md-4 col-lg-4">
    <div class="players-item" data-games="<?= Json::encode(ArrayHelper::getColumn($model->games, 'id')); ?>">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                <?= Html::img($model->getImage()->getUrl('300px'), ['alt' => $model->name, 'class' => 'img-thumbnail']); ?>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                <h4><?= Html::encode($model->name); ?></h4>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                <p><?= Html::encode($model->description); ?></p>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                <?php if ($model->link_a): ?>
                    <?= Html::a('<i class="' . $model->link_a_icon . '"></i>', $model->link_a, ['class' => 'btn btn-default btn-lg', 'target' => '_blank']); ?>
                <?php endif; ?>
                <?php if ($model->link_b): ?>
                    <?= Html::a('<i class="' . $model->link_b_icon . '"></i>', $model->link_b, ['class' => 'btn btn-default btn-lg', 'target' => '_blank']); ?>
                <?php endif; ?>
                <?php if ($model->link_c): ?>
                    <?= Html::a('<i class="' . $model->link_c_icon . '"></i>', $model->link_c, ['class' => 'btn btn-default btn-lg', 'target' => '_blank']); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
