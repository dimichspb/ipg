<?php

/** @var $this View */
/** @var $model Game */

use common\models\Game;
use yii\bootstrap\Html;
use yii\web\View;
?>
<?= Html::a(
    $model->name,
    '#',
    [
        'class' => 'btn btn-default players-choice-btn',
        'data' => [
            'game' => $model->id,
        ]
    ]
); ?>