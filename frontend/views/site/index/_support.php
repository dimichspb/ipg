<?php

/** @var $this View */
/** @var $dataProvider DataProviderInterface */

use common\models\Partner;
use yii\data\DataProviderInterface;
use yii\web\View;
use yii\widgets\ListView;

?>
<div class="site-index-support">
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 text-center">
            <h3>При поддержке</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-2 col-lg-2"></div>
        <div class="col-sm-12 col-md-8 col-lg-8 text-center">
            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => function(Partner $model) {
                    return $this->render('support/_item', [
                        'model' => $model,
                    ]);
                },
                'layout' => "{items}",
            ]); ?>
        </div>
        <div class="col-sm-12 col-md-2 col-lg-2"></div>
    </div>
</div>
