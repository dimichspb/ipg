<?php

/** @var $this View */
/** @var $dataProvider DataProviderInterface */

use common\models\Stream;
use yii\data\DataProviderInterface;
use yii\web\View;
use yii\widgets\ListView;

?>
<div class="site-index-streams">
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 text-center text-sm-center">
            <h3>Новые видео</h3>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-12 text-left">
            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => function(Stream $model) {
                    return $this->render('streams/_item', [
                        'model' => $model,
                    ]);
                },
                'layout' => "{items}",
            ]); ?>
        </div>
    </div>
</div>
