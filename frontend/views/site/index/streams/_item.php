<?php

/** @var $this View */
/** @var $model Stream */

use common\models\Stream;
use yii\bootstrap\Html;
use yii\web\View;

?>
<div class="col-sm-12 col-md-4 col-lg-4">
    <div class="streams-item">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="<?= $model->url; ?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
