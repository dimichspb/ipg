<?php

/** @var $this View */
/** @var $gameDataProvider DataProviderInterface */
/** @var $playerDataProvider DataProviderInterface */

use common\models\Game;
use common\models\Player;
use yii\data\DataProviderInterface;
use yii\web\View;
use yii\widgets\ListView;

$this->registerJs(
<<<JS
$(document).on('click', '.players-choice-btn', function() {
    let button = $(this),
        isActive = button.hasClass('active'),
        buttons = $(".players-choice-btn"),
        players = $(".players-item"),
        game = button.data('game');
    
    buttons.each(function() {
        $(this).removeAttr('active');
    })
    
    button.toggleClass('active', !isActive);
    
    players.each(function() {
        let player = $(this),
            inArray = $.inArray(game, player.data('games')) !== -1;
        
        player.parent('div').toggle((!isActive && inArray) || isActive);
    });
    
    button.blur();
    
    return false;
})
JS
);

?>
<div class="site-index-players">
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 text-center">
            <h3>Состав игроков</h3>
        </div>
    </div>
    <div class="players-choice-btn-container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                <div class="btn-group">
                    <?php foreach ($gameDataProvider->getModels() as $model): ?>
                        <?= $this->render('players/_button', [
                            'model' => $model,
                        ]); ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="players-list-container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 text-left">
                <?= ListView::widget([
                    'dataProvider' => $playerDataProvider,
                    'itemView' => function(Player $model) {
                        return $this->render('players/_item', [
                            'model' => $model,
                        ]);
                    },
                    'layout' => "{items}",
                ]); ?>
            </div>
        </div>
    </div>
</div>
