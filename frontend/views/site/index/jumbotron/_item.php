<?php

/** @var $this View */
/** @var $model News */

use common\models\News;
use yii\bootstrap\Html;
use yii\web\View;

?>
<div class="news-item">
    <div class="row">
        <div class="col-sm-12 col-md-4 col-lg-4 text-left text-sm-center">
            <?= Html::img($model->getImage()->getUrl('300px'), ['alt' => $model->title, 'class' => 'img-thumbnail']); ?>
        </div>
        <div class="col-sm-12 col-md-8 col-lg-8 text-left text-sm-center">
            <h3><?= Html::encode($model->title); ?></h3>
            <span class="date"><?= Yii::$app->formatter->asDate($model->created_at);?></span>
        </div>
    </div>
</div>
