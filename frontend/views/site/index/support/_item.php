<?php

/** @var $this View */
/** @var $model Partner */

use common\models\News;
use common\models\Partner;
use yii\bootstrap\Html;
use yii\web\View;

?>
<div class="col-sm-12 col-md-3 col-lg-3">
    <div class="support-item">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                <?= Html::img($model->getImage()->getUrl('300px'), ['alt' => $model->name, 'class' => 'img-thumbnail']); ?>
            </div>
        </div>
    </div>
</div>
