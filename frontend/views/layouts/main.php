<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;

AppAsset::register($this);

$menuItems = [
    ['label' => 'Главная', 'url' => ['/site/index']],
    ['label' => 'Новости', 'url' => ['/new/index']],
    ['label' => 'Команда', 'url' => ['/team/index']],
    ['label' => 'Стримы', 'url' => ['/stream/index']],
    ['label' => 'Партнёры', 'url' => ['/partner/index']],
    ['label' => 'Матчи', 'url' => ['/match/index']],
];

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrap">
    <?= $this->render('_navbar', [
        'menuItems' => $menuItems,
    ]); ?>

    <?= $this->render('_content', [
        'content' => $content,
    ]); ?>
</div>

<?= $this->render('_footer', [
    'menuItems' => $menuItems,
]); ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
