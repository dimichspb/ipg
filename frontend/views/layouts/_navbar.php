<?php

/** @var $this View */
/** @var $menuItems array */

use yii\bootstrap\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\web\View;

?>
<?php NavBar::begin([
    'brandLabel' => Html::img('/images/logo.svg'),
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar-inverse navbar-fixed-top',
    ],
    'innerContainerOptions' => ['class' => 'container-fluid'],
]); ?>
<?php echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'items' => $menuItems,
]); ?>
<?php NavBar::end(); ?>