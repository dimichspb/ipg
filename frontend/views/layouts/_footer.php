<?php

/** @var $this View */
/** @var $menuItems array */

use yii\bootstrap\Html;
use yii\bootstrap\Nav;
use yii\web\View;

?>
<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-4 col-lg-4 text-left text-sm-center">
                &copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?>
            </div>
            <div class="col-sm-12 col-md-8 col-lg-8 text-right text-sm-center">
                <?php
                echo Nav::widget([
                    'options' => ['class' => 'nav-footer'],
                    'items' => $menuItems,
                ]);
                ?>
            </div>
        </div>
    </div>
</footer>
