<?php

/** @var $this View */
/** @var $content string */

use common\widgets\Alert;
use yii\web\View;
use yii\widgets\Breadcrumbs;
?>
<div class="container-fluid">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
    <?= Alert::widget() ?>
    <?= $content ?>
</div>
