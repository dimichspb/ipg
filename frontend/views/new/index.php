<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ListView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'News');

?>
<div class="news-index">
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-12">
            <?php Pjax::begin(); ?>
            <?= ListView::widget([
                'dataProvider' => $dataProvider,
            ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
