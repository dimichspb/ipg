<?php
namespace frontend\controllers;

use frontend\models\search\GameSearch;
use frontend\models\search\MatchAfterSearch;
use frontend\models\search\MatchBeforeSearch;
use frontend\models\search\MatchSearch;
use frontend\models\search\NewsSearch;
use frontend\models\search\PartnerSearch;
use frontend\models\search\PlayerSearch;
use frontend\models\search\StreamSearch;
use Yii;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $newsJumbotronSearchModel = new NewsSearch();
        $newsJumbotronDataProvider = $newsJumbotronSearchModel->search($this->request->queryParams);

        $matchBeforeSearchModel = new MatchBeforeSearch();
        $matchBeforeDataProvider = $matchBeforeSearchModel->search($this->request->queryParams);

        $matchAfterSearchModel = new MatchAfterSearch();
        $matchAfterDataProvider = $matchAfterSearchModel->search($this->request->queryParams);

        $partnerSearchModel = new PartnerSearch();
        $partnerDataProvider = $partnerSearchModel->search($this->request->queryParams);

        $streamSearchModel = new StreamSearch();
        $streamDataProvider = $streamSearchModel->search($this->request->queryParams);

        $newsSearchModel = new NewsSearch();
        $newsDataProvider = $newsSearchModel->search($this->request->queryParams);

        $gameSearchModel = new GameSearch();
        $gameDataProvider = $gameSearchModel->search($this->request->queryParams);

        $playerSearchModel = new PlayerSearch();
        $playerDataProvider = $playerSearchModel->search($this->request->queryParams);

        return $this->render('index', [
            'newsJumbotronDataProvider' => $newsJumbotronDataProvider,
            'matchBeforeDataProvider' => $matchBeforeDataProvider,
            'matchAfterDataProvider' => $matchAfterDataProvider,
            'partnerDataProvider' => $partnerDataProvider,
            'streamDataProvider' => $streamDataProvider,
            'newsDataProvider' => $newsDataProvider,
            'gameDataProvider' => $gameDataProvider,
            'playerDataProvider' => $playerDataProvider,
        ]);
    }
}
