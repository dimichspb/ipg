<?php

/* @var $this yii\web\View */

$this->title = \Yii::$app->name;
?>
<div class="site-index">
    <div class="body-content">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <?= \Yii::t('app', 'Welcome!'); ?>
            </div>
        </div>
    </div>
</div>
