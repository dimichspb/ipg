<?php

use common\models\Tournament;
use yii\helpers\Html;
use yii\web\View;

/** @var $this View */
/** @var $model Tournament */
/** @var $games array */

$this->title = Yii::t('app', 'Create Tournament');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tournaments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tournament-create">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-sm-12 col-md-8 col-lg-8 text-left">
                    <?= $this->title; ?>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">

                </div>
            </div>
        </div>
        <div class="panel-body">
            <?= $this->render('_form', [
                'model' => $model,
                'games' => $games,
            ]) ?>
        </div>
    </div>
</div>
