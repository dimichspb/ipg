<?php

/** @var $this View */
/** @var $model Player */
/** @var $dataProvider DataProviderInterface */

use common\models\Player;
use yii\data\DataProviderInterface;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\DetailView;

$this->title = Yii::t('app', 'View Player: {name}', [
    'name' => $model->name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Players'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="player-view">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-sm-12 col-md-8 col-lg-8 text-left">
                    <?= $this->title; ?>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-12 col-md-8 col-lg-8">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            //'id',
                            'created_at:datetime',
                            //'created_by',
                            //'updated_at',
                            //'updated_by',
                            //'deleted_at',
                            //'deleted_by',
                            'name',
                            //'description:ntext',
                            'link_a:url',
                            'link_a_icon',
                            'link_b:url',
                            'link_b_icon',
                            'link_c:url',
                            'link_c_icon',
                            //'image',
                            [
                                'attribute' => 'status',
                                'value' => $model->getStatusLabel(),
                            ],
                        ],
                    ]) ?>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4">
                    <?= Html::img($model->getImage()->getUrl('600px'), ['class' => 'img-thumbnail']); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <p><?= $model->description; ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            'name',
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
