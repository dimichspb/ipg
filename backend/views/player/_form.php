<?php

/** @var $this View */
/** @var $model Player */
/** @var $form ActiveForm */
/** @var $dataProvider DataProviderInterface */

use common\models\Game;
use common\models\Player;
use yii\data\DataProviderInterface;
use yii\grid\CheckboxColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
?>

<div class="player-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'imageFile')->fileInput(); ?>

    <?= $form->field($model, 'link_a')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'link_a_icon')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'link_b')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'link_b_icon')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'link_c')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'link_c_icon')->textInput(['maxlength' => true]) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => CheckboxColumn::class,
                'name' => 'Player[game][]',
                'checkboxOptions' => function (Game $game) use ($model) {
                    return [
                        'value' => $game->id,
                        'checked' => $model->isPlayingGame($game),
                    ];
                }
            ],
            'name',
        ]
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
