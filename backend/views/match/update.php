<?php

use common\models\Match;
use yii\helpers\Html;
use yii\web\View;

/** @var $this View */
/** @var $model Match */
/** @var $tournaments array */
/** @var $teams array */

$this->title = Yii::t('app', 'Update Match: {name}', [
    'name' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Matches'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="match-update">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-sm-12 col-md-8 col-lg-8 text-left">
                    <?= $this->title; ?>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">

                </div>
            </div>
        </div>
        <div class="panel-body">
             <?= $this->render('_form', [
                'model' => $model,
                'tournaments' => $tournaments,
                'teams' => $teams,
            ]) ?>
        </div>
    </div>
</div>
