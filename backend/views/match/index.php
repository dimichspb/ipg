<?php

use common\models\Match;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\MatchSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Matches');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="match-index">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-sm-12 col-md-8 col-lg-8 text-left">
                    <?= $this->title; ?>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    <?= Html::a(Yii::t('app', 'Create Match'), ['create'], ['class' => 'btn btn-success']) ?>
                </div>
            </div>
        </div>
        <div class="panel-body">
            <?php Pjax::begin(); ?>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    //'id',
                    'created_at:datetime',
                    //'created_by',
                    //'updated_at',
                    //'updated_by',
                    //'deleted_at',
                    //'deleted_by',
                    //'status',
                    'datetime:datetime',
                    [
                        'attribute' => 'tournament',
                        'value' => function (Match $model) {
                            return $model->tournament_id? $model->tournament->name: null;
                        },
                    ],
                    [
                        'attribute' => 'team_a',
                        'value' => function (Match $model) {
                            return $model->team_a_id? $model->teamA->name: null;
                        },
                    ],
                    [
                        'attribute' => 'team_b',
                        'value' => function (Match $model) {
                            return $model->team_b_id? $model->teamB->name: null;
                        },
                    ],
                    'score_a',
                    'score_b',
                    [
                        'attribute' => 'status',
                        'value' => function (Match $model) {
                            return $model->getStatusLabel();
                        },
                    ],
                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
