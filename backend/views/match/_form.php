<?php

use common\models\Match;
use dosamigos\datetimepicker\DateTimePicker;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/** @var $this View */
/** @var $model Match */
/** @var $tournaments array */
/** @var $teams array */

?>

<div class="match-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, '_datetime')->widget(DateTimePicker::class, [
        'clientOptions' => [
            'autoclose' => true,
        ]
    ]) ?>

    <?= $form->field($model, 'tournament_id')->dropDownList($tournaments) ?>

    <?= $form->field($model, 'team_a_id')->dropDownList($teams) ?>

    <?= $form->field($model, 'team_b_id')->dropDownList($teams) ?>

    <?= $form->field($model, 'score_a')->textInput() ?>

    <?= $form->field($model, 'score_b')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
