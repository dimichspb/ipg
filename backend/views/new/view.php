<?php

use yii\helpers\Html;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\News */

$this->title = Yii::t('app', 'View News: {name}', [
    'name' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'News'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
YiiAsset::register($this);

?>
<div class="news-view">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-sm-12 col-md-8 col-lg-8 text-left">
                    <?= $this->title; ?>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-12 col-md-8 col-lg-8">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            //'id',
                            'created_at:datetime',
                            //'created_by',
                            //'updated_at',
                            //'updated_by',
                            //'deleted_at',
                            //'deleted_by',
                            'title',
                            [
                                'attribute' => 'status',
                                'value' => $model->getStatusLabel(),
                            ],
                        ],
                    ]) ?>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4">
                    <?= Html::img($model->getImage()->getUrl('600px'), ['class' => 'img-thumbnail']); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-8 col-lg-8">
                    <p><?= $model->body; ?></p>
                </div>
            </div>
        </div>
    </div>
</div>
