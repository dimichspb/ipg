<?php

/** @var $this \yii\web\View */

use dmstr\widgets\Menu;

?>
<aside class="main-sidebar">
    <section class="sidebar">
        <?= Menu::widget([
            'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
            'items' => [
                //['label' => \Yii::t('app', 'Content Management'), 'options' => ['class' => 'header']],
                ['label' => \Yii::t('app', 'Users'), 'icon' => 'users', 'url' => ['user/index']],
                ['label' => \Yii::t('app', 'News'), 'icon' => 'newspaper-o', 'url' => ['new/index']],
                ['label' => \Yii::t('app', 'Players'), 'icon' => 'user-secret', 'url' => ['player/index']],
                ['label' => \Yii::t('app', 'Streams'), 'icon' => 'video-camera', 'url' => ['stream/index']],
                ['label' => \Yii::t('app', 'Partners'), 'icon' => 'globe', 'url' => ['partner/index']],
                ['label' => \Yii::t('app', 'Games'), 'icon' => 'gamepad', 'url' => ['game/index']],
                ['label' => \Yii::t('app', 'Teams'), 'icon' => 'user-plus', 'url' => ['team/index']],
                ['label' => \Yii::t('app', 'Tournaments'), 'icon' => 'star-half-o', 'url' => ['tournament/index']],
                ['label' => \Yii::t('app', 'Matches'), 'icon' => 'futbol-o', 'url' => ['match/index']],
            ],
        ]
        ) ?>
    </section>
</aside>
