<?php

use common\models\Game;
use yii\helpers\Html;
use yii\web\View;

/** @var $this View */
/** @var $model Game */

$this->title = Yii::t('app', 'Update Game: {name}', [
    'name' => $model->name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Games'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="game-update">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-sm-12 col-md-8 col-lg-8 text-left">
                    <?= $this->title; ?>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">

                </div>
            </div>
        </div>
        <div class="panel-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>
