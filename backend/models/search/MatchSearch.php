<?php

namespace backend\models\search;

use common\models\Team;
use common\models\Tournament;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Match;

/**
 * MatchSearch represents the model behind the search form of `common\models\Match`.
 */
class MatchSearch extends Model
{
    public $id;
    public $status;
    public $created_at;
    public $created_by;
    public $updated_at;
    public $updated_by;
    public $deleted_at;
    public $deleted_by;
    public $datetime;
    public $tournament_id;
    public $tournament;
    public $team_a_id;
    public $team_a;
    public $team_b_id;
    public $team_b;
    public $score_a;
    public $score_b;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'created_by', 'updated_at', 'updated_by', 'deleted_at', 'deleted_by', 'status', 'datetime', 'tournament_id', 'tournament', 'team_a_id', 'team_a', 'team_b_id', 'team_b', 'score_a', 'score_b'], 'integer'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search(array $params)
    {
        $query = Match::find();
        $query->leftJoin(['tournament' => Tournament::tableName()], Match::tableName() . '.tournament_id = tournament.id');
        $query->leftJoin(['team_a' => Team::tableName()], Match::tableName() . '.team_a_id = team_a.id');
        $query->leftJoin(['team_b' => Team::tableName()], Match::tableName() . '.team_b_id = team_b.id');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
            'status' => $this->status,
            'datetime' => $this->datetime,
            'tournament_id' => $this->tournament_id,
            'team_a_id' => $this->team_a_id,
            'team_b_id' => $this->team_b_id,
            'score_a' => $this->score_a,
            'score_b' => $this->score_b,
        ]);

        $query
            ->andFilterWhere([
                'ilike', Tournament::tableName() . '.name', $this->tournament
            ])
            ->andFilterWhere([
                'ilike', 'team_a.name', $this->team_a
            ])
            ->andFilterWhere([
                'ilike', 'team_b.name', $this->team_b
            ])
        ;

        $dataProvider->sort->attributes['tournament'] = [
            'asc' => ['tournament.name' => SORT_ASC],
            'desc' => ['tournament.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['team_a'] = [
            'asc' => ['team_a.name' => SORT_ASC],
            'desc' => ['team_a.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['team_b'] = [
            'asc' => ['team_a.name' => SORT_ASC],
            'desc' => ['team_b.name' => SORT_DESC],
        ];

        $dataProvider->sort->defaultOrder = ['created_at' => SORT_ASC];

        return $dataProvider;
    }
}
