<?php

namespace backend\models\search;

use common\models\Game;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Tournament;

/**
 * TournamentSearch represents the model behind the search form of `common\models\Tournament`.
 */
class TournamentSearch extends Model
{
    public $id;
    public $status;
    public $created_at;
    public $created_by;
    public $updated_at;
    public $updated_by;
    public $deleted_at;
    public $deleted_by;
    public $name;
    public $image;
    public $game_id;
    public $game;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'created_by', 'updated_at', 'updated_by', 'deleted_at', 'deleted_by', 'status', 'game_id'], 'integer'],
            [['name', 'image', 'game',], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search(array $params)
    {
        $query = Tournament::find();
        $query->leftJoin(['game' => Game::tableName()], Tournament::tableName() . '.game_id = game.id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
            'status' => $this->status,
            'game_id' => $this->game_id,
        ]);

        $query
            ->andFilterWhere(['ilike', 'name', $this->name])
            ->andFilterWhere(['ilike', 'image', $this->image])
            ->andFilterWhere(['ilike', 'game.name', $this->game]);

        $dataProvider->sort->attributes['game'] = [
            'asc' => ['game.name' => SORT_ASC],
            'desc' => ['game.name' => SORT_DESC],
        ];

        $dataProvider->sort->defaultOrder = ['created_at' => SORT_ASC];

        return $dataProvider;
    }
}
