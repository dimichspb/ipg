<?php
namespace common\bootstrap;

use yii\base\Application;
use yii\base\BootstrapInterface;
use yii\di\Container;
use yii\rbac\ManagerInterface;

class Bootstrap implements BootstrapInterface
{
    public function bootstrap($app)
    {
        $container = \Yii::$container;

        $container->set(ManagerInterface::class, \Yii::$app->authManager);
    }
}