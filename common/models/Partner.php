<?php

namespace common\models;

use common\models\traits\CreatedByTrait;
use common\models\traits\DefaultModelBehaviorsTrait;
use common\models\traits\DeletedByTrait;
use common\models\traits\ImageTrait;
use common\models\traits\IsActiveTrait;
use common\models\traits\IsDeletedTrait;
use common\models\traits\StatusTrait;
use common\models\traits\UpdatedByTrait;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%partner}}".
 *
 * @property int $id
 * @property int|null $created_at
 * @property int|null $created_by
 * @property int|null $updated_at
 * @property int|null $updated_by
 * @property int|null $deleted_at
 * @property int|null $deleted_by
 * @property int|null $status
 * @property string|null $name
 * @property string|null $description
 * @property string|null $url
 * @property string|null $link_a
 * @property string|null $link_b
 * @property string|null $image
 */
class Partner extends ActiveRecord
{
    use DefaultModelBehaviorsTrait,
        IsActiveTrait,
        IsDeletedTrait,
        StatusTrait,
        CreatedByTrait,
        UpdatedByTrait,
        ImageTrait,
        DeletedByTrait;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%partner}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'created_by', 'updated_at', 'updated_by', 'deleted_at', 'deleted_by', 'status'], 'default', 'value' => null],
            [['created_at', 'created_by', 'updated_at', 'updated_by', 'deleted_at', 'deleted_by', 'status'], 'integer'],
            [['description'], 'string'],
            [['name', 'url', 'link_a', 'link_b', 'image'], 'string', 'max' => 255],
            ['status', 'default', 'value' => Status::STATUS_ACTIVE],
            ['status', 'in', 'range' => array_keys(Status::getStatusLabels())],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
            [['deleted_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['deleted_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_at' => Yii::t('app', 'Created at'),
            'created_by' => Yii::t('app', 'Created by'),
            'updated_at' => Yii::t('app', 'Updated at'),
            'updated_by' => Yii::t('app', 'Updated by'),
            'deleted_at' => Yii::t('app', 'Deleted at'),
            'deleted_by' => Yii::t('app', 'Deleted by'),
            'status' => Yii::t('app', 'Status'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'url' => Yii::t('app', 'Url'),
            'link_a' => Yii::t('app', 'Link A'),
            'link_b' => Yii::t('app', 'Link B'),
            'image' => Yii::t('app', 'Image'),
        ];
    }
}
