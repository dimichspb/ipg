<?php

namespace common\models\traits;

use common\models\Status;

/**
 * Trait IsActiveTrait
 * @package common\models\traits
 *
 * @property integer $status
 */
trait IsActiveTrait
{
    public function isActive()
    {
        return $this->status === Status::STATUS_ACTIVE;
    }
}