<?php
namespace common\models\traits;

use common\models\User;
use yii\db\ActiveQuery;

/**
 * Trait DeletedByTrait
 * @package common\models\traits
 */
trait DeletedByTrait
{
    /**
     * Gets query for [[DeletedBy]].
     *
     * @return ActiveQuery
     */
    public function getDeletedBy()
    {
        return $this->hasOne(User::class, ['id' => 'deleted_by']);
    }
}