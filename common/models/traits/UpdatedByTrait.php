<?php
namespace common\models\traits;

use common\models\User;
use yii\db\ActiveQuery;

/**
 * Trait UpdatedByTrait
 * @package common\models\traits
 */
trait UpdatedByTrait
{
    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'updated_by']);
    }
}