<?php
namespace common\models\traits;

use rico\yii2images\models\Image;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * Trait ImageTrait
 * @package common\models\traits
 *
 * @method bool|Image attachImage(string $path, bool $main)
 * @method null|ActiveRecord getImage()
 * @method ActiveRecord[] getImages()
 */
trait ImageTrait
{
    /**
     * @var UploadedFile
     */
    public $imageFile;

    public function rules()
    {
        $rules = static::rules();

        $rules[] = [['imageFile',], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'];

        return $rules;
    }

    public function upload()
    {
        if (!$this->validate()) {
            return false;
        }
        if ($this->imageFile) {
            $path = \Yii::getAlias('@common/storage/' . $this->imageFile->baseName . '.' . $this->imageFile->extension);
            $this->imageFile->saveAs($path);
            $this->attachImage($path, true);
        }

        return true;
    }
}