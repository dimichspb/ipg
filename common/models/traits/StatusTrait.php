<?php
namespace common\models\traits;

use common\models\Status;

/**
 * Trait StatusTrait
 * @package common\models\traits
 *
 * @property integer $status
 */
trait StatusTrait
{
    public function getStatusLabel()
    {
        return Status::getStatusLabel($this->status);
    }
}