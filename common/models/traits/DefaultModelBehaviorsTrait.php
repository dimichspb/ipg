<?php

namespace common\models\traits;

use common\models\Status;
use rico\yii2images\behaviors\ImageBehave;
use yii\base\InvalidConfigException;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use yii2tech\ar\softdelete\SoftDeleteQueryBehavior;

trait DefaultModelBehaviorsTrait
{
    /**
     * @return array
     * @throws InvalidConfigException
     */
    public function behaviors()
    {
        try {
            $user = \Yii::$app->get('user');
        } catch (InvalidConfigException $exception) {
            $user = null;
        }
        return [
            [
                'class' => TimestampBehavior::class,
            ],
            [
                'class' => BlameableBehavior::class,
            ],
            [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'status' => Status::STATUS_DELETED,
                    'deleted_at' => (new \DateTime())->getTimestamp(),
                    'deleted_by' => $user? $user->getId(): null,
                ],
                'restoreAttributeValues' => [
                    'status' => Status::STATUS_ACTIVE,
                ],
                'replaceRegularDelete' => true
            ],
            [
                'class' => ImageBehave::class,
            ]
        ];
    }

    /**
     * @return ActiveQuery|SoftDeleteQueryBehavior
     */
    public static function find()
    {
        $query = parent::find();
        $query->attachBehavior('softDelete', SoftDeleteQueryBehavior::class);
        return $query;
    }
}