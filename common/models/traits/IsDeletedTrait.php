<?php

namespace common\models\traits;

use common\models\Status;

/**
 * Trait IsDeletedTrait
 * @package common\models\traits
 *
 * @property integer $status
 */
trait IsDeletedTrait
{
    public function isDeleted()
    {
        return $this->status === Status::STATUS_DELETED;
    }
}