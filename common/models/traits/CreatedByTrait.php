<?php
namespace common\models\traits;

use common\models\User;
use yii\db\ActiveQuery;

/**
 * Trait CreatedByTrait
 * @package common\models\traits
 */
trait CreatedByTrait
{
    /**
     * Gets query for [[CreatedBy]].
     *
     * @return ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }
}