<?php

namespace common\models;

use common\models\traits\CreatedByTrait;
use common\models\traits\DefaultModelBehaviorsTrait;
use common\models\traits\DeletedByTrait;
use common\models\traits\IsActiveTrait;
use common\models\traits\IsDeletedTrait;
use common\models\traits\StatusTrait;
use common\models\traits\UpdatedByTrait;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the model class for table "{{%game}}".
 *
 * @property int $id
 * @property int|null $created_at
 * @property int|null $created_by
 * @property int|null $updated_at
 * @property int|null $updated_by
 * @property int|null $deleted_at
 * @property int|null $deleted_by
 * @property int|null $status
 * @property string|null $name
 * @property string|null $image
 *
 * @property Tournament[] $tournaments
 * @property PlayerGame[] $playerGames
 * @property Player[] $players
 */
class Game extends ActiveRecord
{
    use DefaultModelBehaviorsTrait,
        IsActiveTrait,
        IsDeletedTrait,
        StatusTrait,
        CreatedByTrait,
        UpdatedByTrait,
        DeletedByTrait;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%game}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'created_by', 'updated_at', 'updated_by', 'deleted_at', 'deleted_by', 'status'], 'default', 'value' => null],
            [['created_at', 'created_by', 'updated_at', 'updated_by', 'deleted_at', 'deleted_by', 'status'], 'integer'],
            [['name', 'image'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
            [['deleted_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['deleted_by' => 'id']],
            ['status', 'default', 'value' => Status::STATUS_ACTIVE],
            ['status', 'in', 'range' => array_keys(Status::getStatusLabels())],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_at' => Yii::t('app', 'Created at'),
            'created_by' => Yii::t('app', 'Created by'),
            'updated_at' => Yii::t('app', 'Updated at'),
            'updated_by' => Yii::t('app', 'Updated by'),
            'deleted_at' => Yii::t('app', 'Deleted at'),
            'deleted_by' => Yii::t('app', 'Deleted by'),
            'status' => Yii::t('app', 'Status'),
            'name' => Yii::t('app', 'Name'),
            'image' => Yii::t('app', 'Image'),
        ];
    }

    /**
     * Gets query for [[Tournaments]].
     *
     * @return ActiveQuery
     */
    public function getTournaments()
    {
        return $this->hasMany(Tournament::class, ['game_id' => 'id']);
    }

    /**
     * Gets query for [[PlayerGames]].
     *
     * @return ActiveQuery
     */
    public function getPlayerGames()
    {
        return $this->hasMany(PlayerGame::class, ['game_id' => 'id']);
    }

    /**
     * Gets query for [[Players]].
     *
     * @return ActiveQuery
     */
    public function getPlayers()
    {
        return $this->hasMany(Player::class, ['id' => 'player_id'])->via('playerGames');
    }

}
