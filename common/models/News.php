<?php

namespace common\models;

use common\models\traits\CreatedByTrait;
use common\models\traits\DefaultModelBehaviorsTrait;
use common\models\traits\DeletedByTrait;
use common\models\traits\ImageTrait;
use common\models\traits\IsActiveTrait;
use common\models\traits\IsDeletedTrait;
use common\models\traits\StatusTrait;
use common\models\traits\UpdatedByTrait;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%new}}".
 *
 * @property int $id
 * @property int|null $created_at
 * @property int|null $created_by
 * @property int|null $updated_at
 * @property int|null $updated_by
 * @property int|null $deleted_at
 * @property int|null $deleted_by
 * @property int|null $status
 * @property string|null $title
 * @property string|null $body
 *
 * @property User $createdBy
 * @property User $updatedBy
 * @property User $deletedBy
 */
class News extends ActiveRecord
{
    use DefaultModelBehaviorsTrait,
        IsActiveTrait,
        IsDeletedTrait,
        StatusTrait,
        CreatedByTrait,
        UpdatedByTrait,
        DeletedByTrait,
        ImageTrait;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%new}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'created_by', 'updated_at', 'updated_by', 'deleted_at', 'deleted_by', 'status'], 'default', 'value' => null],
            [['created_at', 'created_by', 'updated_at', 'updated_by', 'deleted_at', 'deleted_by', 'status'], 'integer'],
            [['body'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
            [['deleted_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['deleted_by' => 'id']],
            ['status', 'default', 'value' => Status::STATUS_ACTIVE],
            ['status', 'in', 'range' => array_keys(Status::getStatusLabels())],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_at' => Yii::t('app', 'Created at'),
            'created_by' => Yii::t('app', 'Created by'),
            'updated_at' => Yii::t('app', 'Updated at'),
            'updated_by' => Yii::t('app', 'Updated by'),
            'deleted_at' => Yii::t('app', 'Deleted at'),
            'deleted_by' => Yii::t('app', 'Deleted by'),
            'status' => Yii::t('app', 'Status'),
            'title' => Yii::t('app', 'Title'),
            'body' => Yii::t('app', 'Body'),
        ];
    }
}
