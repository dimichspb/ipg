<?php

namespace common\models;

use common\models\traits\CreatedByTrait;
use common\models\traits\DefaultModelBehaviorsTrait;
use common\models\traits\DeletedByTrait;
use common\models\traits\ImageTrait;
use common\models\traits\IsActiveTrait;
use common\models\traits\IsDeletedTrait;
use common\models\traits\StatusTrait;
use common\models\traits\UpdatedByTrait;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%player}}".
 *
 * @property int $id
 * @property int|null $created_at
 * @property int|null $created_by
 * @property int|null $updated_at
 * @property int|null $updated_by
 * @property int|null $deleted_at
 * @property int|null $deleted_by
 * @property int|null $status
 * @property string|null $name
 * @property string|null $description
 * @property string|null $image
 * @property string|null $link_a
 * @property string|null $link_b
 * @property string|null $link_c
 * @property string|null $link_a_icon
 * @property string|null $link_b_icon
 * @property string|null $link_c_icon
 *
 * @property User $createdBy
 * @property User $updatedBy
 * @property User $deletedBy
 * @property PlayerGame[] $playerGames
 * @property Game[] $games
 */
class Player extends ActiveRecord
{
    use DefaultModelBehaviorsTrait,
        IsActiveTrait,
        IsDeletedTrait,
        StatusTrait,
        CreatedByTrait,
        UpdatedByTrait,
        DeletedByTrait,
        ImageTrait;

    const DEFAULT_LINK_A_ICON = 'fa fa-vk';
    const DEFAULT_LINK_B_ICON = 'fa fa-twitter';
    const DEFAULT_LINK_C_ICON = 'fa fa-facebook-f';

    /**
     * @var Game[]
     */
    private $_games = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%player}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'created_by', 'updated_at', 'updated_by', 'deleted_at', 'deleted_by', 'status'], 'default', 'value' => null],
            [['created_at', 'created_by', 'updated_at', 'updated_by', 'deleted_at', 'deleted_by', 'status'], 'integer'],
            [['description'], 'string'],
            [['name', 'image', 'link_a', 'link_b', 'link_c', 'link_a_icon', 'link_b_icon', 'link_c_icon'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
            [['deleted_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['deleted_by' => 'id']],
            ['status', 'default', 'value' => Status::STATUS_ACTIVE],
            ['link_a_icon', 'default', 'value' => self::DEFAULT_LINK_A_ICON],
            ['link_b_icon', 'default', 'value' => self::DEFAULT_LINK_B_ICON],
            ['link_c_icon', 'default', 'value' => self::DEFAULT_LINK_C_ICON],
            ['status', 'in', 'range' => array_keys(Status::getStatusLabels())],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_at' => Yii::t('app', 'Created at'),
            'created_by' => Yii::t('app', 'Created by'),
            'updated_at' => Yii::t('app', 'Updated at'),
            'updated_by' => Yii::t('app', 'Updated by'),
            'deleted_at' => Yii::t('app', 'Deleted at'),
            'deleted_by' => Yii::t('app', 'Deleted by'),
            'status' => Yii::t('app', 'Status'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'image' => Yii::t('app', 'Image'),
            'link_a' => Yii::t('app', 'Link A'),
            'link_b' => Yii::t('app', 'Link B'),
            'link_c' => Yii::t('app', 'Link C'),
            'link_a_icon' => Yii::t('app', 'Link A Icon'),
            'link_b_icon' => Yii::t('app', 'Link B Icon'),
            'link_c_icon' => Yii::t('app', 'Link C Icon'),
        ];
    }

    /**
     * Gets query for [[PlayerGames]].
     *
     * @return ActiveQuery
     */
    public function getPlayerGames()
    {
        return $this->hasMany(PlayerGame::class, ['player_id' => 'id']);
    }

    /**
     * Gets query for [[Games]].
     *
     * @return ActiveQuery
     */
    public function getGames()
    {
        return $this->hasMany(Game::class, ['id' => 'game_id'])->via('playerGames');
    }

    public function startPlayingGame(Game $game)
    {
        $playerGame = new PlayerGame();
        $playerGame->player_id = $this->id;
        $playerGame->game_id = $game->id;

        return $playerGame->save();
    }

    public function stopPlayingGame(Game $game)
    {
        $playerGame = PlayerGame::find()->where([
            'player_id' => $this->id,
            'game_id' => $game->id,
        ])->one();

        $playerGame->delete();
    }

    public function wantsPlayingGame(Game $game)
    {
        return in_array($game->id, ArrayHelper::getColumn($this->_games, 'id'));
    }

    public function isPlayingGame(Game $game)
    {
        return in_array($game->id, ArrayHelper::getColumn($this->games, 'id'));
    }

    public function load($data, $formName = null)
    {
        $scope = $formName === null ? $this->formName() : $formName;
        if ($scope === '' && !empty($data['game'])) {
            $this->_games = Game::find()->where(['in', 'id', $data['game']])->all();
        } elseif (isset($data[$scope])) {
            $this->_games = Game::find()->where(['in', 'id', $data[$scope]['game']])->all();
        }

        return parent::load($data, $formName);
    }

    public function beforeSave($insert)
    {
        foreach($this->games as $game) {
            if (!$this->wantsPlayingGame($game)) {
                $this->stopPlayingGame($game);
            }
        }

        foreach($this->_games as $game) {
            if (!$this->isPlayingGame($game)) {
                $this->startPlayingGame($game);
            }
        }

        return parent::beforeSave($insert);
    }
}
