<?php

namespace common\models;

class Status
{
    const STATUS_ACTIVE = 0;
    const STATUS_INACTIVE = 90;
    const STATUS_DELETED = 100;

    public static function getStatusLabels()
    {
        return [
            self::STATUS_ACTIVE => \Yii::t('app', 'Active'),
            self::STATUS_INACTIVE => \Yii::t('app', 'Inactive'),
            self::STATUS_DELETED => \Yii::t('app', 'Deleted'),
        ];
    }

    public static function getStatusLabel(int $status)
    {
        $labels = self::getStatusLabels();

        if (!isset($labels[$status])) {
            throw new \InvalidArgumentException(\Yii::t('app', 'Status "{status}" not pre-defined', [
                'status' => $status,
            ]));
        }

        return $labels[$status];
    }
}