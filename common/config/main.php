<?php

use common\bootstrap\Bootstrap;
use rico\yii2images\Module;
use yii\caching\FileCache;
use yii\rbac\DbManager;

return [
    'name' => 'Imperial Pro Gaming',
    'language' => 'ru_RU',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'bootstrap' => [
        Bootstrap::class,
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => FileCache::class,
        ],
        'authManager' => [
            'class' => DbManager::class,
        ],
    ],
    'modules' => [
        'yii2images' => [
            'class' => Module::class,
            //be sure, that permissions ok
            //if you cant avoid permission errors you have to create "images" folder in web root manually and set 777 permissions
            'imagesStorePath' => '@common/storage', //path to origin images
            'imagesCachePath' => '@common/storage/cache', //path to resized copies
            'graphicsLibrary' => 'GD', //but really its better to use 'Imagick'
            'placeHolderPath' => '@common/storage/placeholder.jpg', // if you want to get placeholder when image not exists, string will be processed by Yii::getAlias
        ],

    ]
];
